﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author Jemma Thompson
public class IsWilted : MonoBehaviour {
    public DestroyPlatform destroyPlatform;
	// Use this for initialization
	void Start () {
        destroyPlatform = this.gameObject.GetComponentInParent<DestroyPlatform>();
	}
    public void Wilt()
    {
        destroyPlatform.isWilted = true;
    }
}
