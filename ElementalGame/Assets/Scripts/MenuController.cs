﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
// Written by Brodie Pirovich

public class MenuController : MonoBehaviour {
    public GameObject settingsMenu;
    public GameObject mainMenu;
    public GameObject eventSystem;

    public void PlayButton()
    {
        SceneManager.LoadScene(1);
    }
    public void ExitButton()
    {
        Application.Quit();
    }

    public void SettingsButton()
    {
        settingsMenu.gameObject.SetActive(true);
        mainMenu.gameObject.SetActive(false);
        eventSystem.gameObject.SetActive(false);


    }

    public void SettingsExitButton()
    {
        settingsMenu.gameObject.SetActive(false);
        mainMenu.gameObject.SetActive(true);
        eventSystem.gameObject.SetActive(true);

    }
}
