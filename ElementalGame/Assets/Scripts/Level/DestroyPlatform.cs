﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class DestroyPlatform : MonoBehaviour
{
    [SerializeField]
    private float maxTime = 7f;
    public float currentTime;
    public ElementalPowers powersScript;
    public Component[] meshColliders;
    public Renderer[] leafRenderer;
    public Animator[] leafAnimators;
    public bool isWilted = false;
    // Use this for initialization
    void Start()
    {
        powersScript = GameObject.Find("ameria").GetComponent<ElementalPowers>();
        
    }
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= 5f)
        {           
            leafRenderer = this.gameObject.GetComponentsInChildren<Renderer>();
            StartCoroutine("FlashRed");
        }
        if (currentTime >= maxTime)
        {


            meshColliders = this.gameObject.GetComponentsInChildren<MeshCollider>();
            leafRenderer = this.gameObject.GetComponentsInChildren<Renderer>();
            leafAnimators = this.gameObject.GetComponentsInChildren<Animator>();
            foreach (MeshCollider collider in meshColliders)
            {
                collider.enabled = false;
            }
            foreach (Renderer renderer in leafRenderer)
            {
                renderer.material = powersScript.leafWilt;

            }
            foreach (Animator animations in leafAnimators)
            {
                animations.Play("Wilt");
            }

            if (isWilted)
            {
                if (this.gameObject.tag == "Leaf")
                {
                    powersScript.currentEarthAmount++;
                }
                else if (this.gameObject.tag == "Leaf1")
                {
                    powersScript.currentEarthAmount++;
                }
                else if (this.gameObject.tag == "Leaf2")
                {
                    powersScript.currentEarthAmount += 2;
                }
                else if (this.gameObject.tag == "Leaf3")
                {
                    powersScript.currentEarthAmount += 3;
                }
                ShiftArray();
                ShiftTag();
                DestroyPlant();
            }

        }
    }
    public void ShiftArray()
    {
        powersScript.leafPlant[0] = powersScript.leafPlant[1];
        powersScript.leafPlant[1] = powersScript.leafPlant[2];
        powersScript.leafPlant[2] = null;
    }
    public void ShiftTag()
    {
        if (powersScript.leafPlant[0] != null && (powersScript.leafPlant[0].tag == "Leaf" || powersScript.leafPlant[0].tag == "Leaf1"))
        {
            foreach (Transform child in powersScript.leafPlant[0].transform.GetComponentsInChildren<Transform>())
            {
                child.tag = "Leaf";
            }
        }
        if (powersScript.leafPlant[1] != null && (powersScript.leafPlant[1].tag == "Leaf" || powersScript.leafPlant[1].tag == "Leaf1"))
        {
            foreach (Transform child in powersScript.leafPlant[1].transform.GetComponentsInChildren<Transform>())
            {
                child.tag = "Leaf1";
            }
        }
    }
    public void DestroyPlant()
    {

        Destroy(this.gameObject);
    }
    public IEnumerator FlashRed()
    {
        foreach (Renderer renderer in leafRenderer)
        {
            for (int i = 0; i < 4; i++)
            {
                renderer.material.color = Color.red;
                yield return new WaitForSeconds(0.1f);
                renderer.material.color = Color.white;
                yield return new WaitForSeconds(0.1f);
            }
        }
    }
}