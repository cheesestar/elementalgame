﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class VanishingPlatform : MonoBehaviour
{
    public Rigidbody myRB;
    public float myTimer;
    public int tillFall = 2;
    public bool playerOn;
    public Vector3 myPos;
    // Use this for initialization
    void Start()
    {
        myRB = GetComponent<Rigidbody>();
        playerOn = false;
        myPos = this.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerOn == true)
        {
            myTimer += Time.deltaTime;
            if (myTimer >= tillFall)
            {
                myRB.isKinematic = false;
                playerOn = false;
                Invoke("MoveBack", 15f);
            }
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerOn = true;
        }
    }
    public void MoveBack()
    {
        myRB.isKinematic = true;
        this.gameObject.transform.position = myPos;
    }
}
