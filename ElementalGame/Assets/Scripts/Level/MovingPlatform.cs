﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class MovingPlatform : MonoBehaviour {

    public Vector3 startPosition;
    public Vector3 endPosition;
    public Vector3 newPosition;
    public string currentState;
    public float mySpeed;
    public float resetTime;
    // Use this for initialization
    void Start () {
        ChangeTarget();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        this.gameObject.transform.position = Vector3.Lerp(this.transform.position,newPosition,mySpeed *Time.deltaTime);
	}

    void ChangeTarget()
    {
        if (currentState == "Moving to start")
        {
            currentState = "Moving to end";
            newPosition = endPosition;
        }
        else if(currentState == "Moving to end")
        {
            currentState = "Moving to start";
            newPosition = startPosition;
        }
        else if (currentState == "")
        {
            currentState = "Moving to end";
            newPosition = endPosition;
        }
        Invoke("ChangeTarget",resetTime);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(startPosition, 0.25f);
        Gizmos.DrawSphere(endPosition, 0.25f);
    }
}
