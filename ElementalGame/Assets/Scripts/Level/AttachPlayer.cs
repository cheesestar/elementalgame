﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class AttachPlayer : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        other.transform.parent = gameObject.transform;
        Debug.Log("Attached");
    }
    private void OnTriggerExit(Collider other)
    {
        other.transform.parent = null;
        Debug.Log("Dettached");
    }
}
