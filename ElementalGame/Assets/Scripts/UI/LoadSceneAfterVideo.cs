﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
// Written by Brodie Pirovich

public class LoadSceneAfterVideo : MonoBehaviour {

    public VideoPlayer videoPlayer; // Drag & Drop the GameObject holding the VideoPlayer component
    public string sceneName;

    void Start()
    {
        videoPlayer.loopPointReached += EndReached;
    }

    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
    }
}

