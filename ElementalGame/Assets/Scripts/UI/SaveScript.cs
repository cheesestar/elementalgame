﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
// Written by Brodie Pirovich

public static class SaveScript
{

    //public static List<Game> savedGames = new List<game>();

    //it's static so we can call it from anywhere
    public static void Save(InventoryScript inventory)
    {
        //SaveScript.savedGames.Add(Game.current);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd");
        PlayerData data = new PlayerData(inventory);

        bf.Serialize(file, data);
        file.Close();
    }

    public static void SaveCheckpoint(HealthSystem checkpointSystem)
    {
        //SaveScript.savedGames.Add(Game.current);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedCheckpoints.gd");
        CheckpointData checkpointData = new CheckpointData(checkpointSystem);

        bf.Serialize(file, checkpointData);
        file.Close();
    }

    public static void SaveLevel(HealthSystem healthSystem)
    {
        //SaveScript.savedGames.Add(Game.current);
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedLevel.gd");
        LevelData levelData = new LevelData(healthSystem);

        bf.Serialize(file, levelData);
        file.Close();
    }

    public static float[] Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);

            PlayerData data = bf.Deserialize(file) as PlayerData;
            file.Close();

            return data.stats;
        }
        else
        {
            Debug.LogError("File does not exist");
            return new float[5];
        }
    }

    public static int[] LoadLevel()
    {
        if (File.Exists(Application.persistentDataPath + "/savedLevel.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedLevel.gd", FileMode.Open);

            LevelData LevelData = bf.Deserialize(file) as LevelData;
            file.Close();

            return LevelData.levelStats;
        }
        else
        {
            Debug.LogError("File does not exist");
            return new int[2];
        }
    }

    public static Transform[] LoadCheckpoint()
    {
        if (File.Exists(Application.persistentDataPath + "/savedCheckpoints.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedCheckpoints.gd", FileMode.Open);

            CheckpointData checkpointData = bf.Deserialize(file) as CheckpointData;
            file.Close();

            return checkpointData.checkpoint;
        }
        else
        {
            Debug.LogError("File does not exist");
            return new Transform[1];
        }
    }
}

[Serializable]
public class PlayerData
{
    public float[] stats;

    public PlayerData(InventoryScript inventory)
    {
        stats = new float[5];
        stats[0] = inventory.coins;
        stats[1] = inventory.gems;
        stats[2] = inventory.windKey;
        stats[3] = inventory.earthKey;
        stats[4] = inventory.level;
    }
}

[Serializable]
public class LevelData
{
    public int[] levelStats;

    public LevelData(HealthSystem healthSystem)
    {
        levelStats = new int[2];
        levelStats[0] = healthSystem.currentHitpoints;
        levelStats[1] = healthSystem.livesCount;
    }
}

[Serializable]
public class CheckpointData
{
    public Transform[] checkpoint;

    public CheckpointData(HealthSystem checkpointSystem)
    {
        checkpoint = new Transform[1];
        checkpoint[0] = checkpointSystem.myCheckpoint;
    }
}

