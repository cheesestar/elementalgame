﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
// Written by Brodie Pirovich

public class MenuManager : MonoBehaviour {
    [SerializeField]
    private bool isPaused;

    [SerializeField]
    private GameObject menu;

    public CameraController myCamera;

    void Start()
    {
        //menu = GameObject.FindGameObjectWithTag("Menu");
        //myCamera = GameObject.Find("MainCamera").GetComponent<CameraController>();

    }

    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Pause"))
        {
            isPaused = !isPaused;
        }

        if (isPaused)
        {
            Pause();
        }

        else
        {
            Resume();
        }
    }

    public void Pause()
    {
        Time.timeScale = 0;
        menu.SetActive(true);
        //myCamera.gameObject.SetActive(false);
        //pauseEnabled = true;
    }

    public void Resume()
    {
        menu.SetActive(false);
        Time.timeScale = 1;
        //myCamera.gameObject.SetActive(true);
        isPaused = false;
    }

    public void Exit()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void EndScreen()
    {
        SceneManager.LoadScene("EndScreen");
    }
}

