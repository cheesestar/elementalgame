﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
// Written by Brodie Pirovich

public class LevelLoader : MonoBehaviour {
    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;

public void LoadLevel (string sceneName)
    {
        StartCoroutine(LoadAsynchronusly(sceneName));
    }

    IEnumerator LoadAsynchronusly (string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            slider.value = progress;
            progressText.text = progress * 100f + "%";
            yield return null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "ameria")
        {
            LoadLevel("AirLevelGreyBox");
        }
    }

}
