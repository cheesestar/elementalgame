﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
// Written by Brodie Pirovich

public class EnemyClass : MonoBehaviour
{
    //A reference to the player.
    public GameObject thePlayer;
    // A reference to the script that stores player's hitpoints.
    public HealthSystem playerHealth;
    //The distance that the player is away from the enemy.
    public float targetDistance;
    //The range at which the enemy will follow the player.
    public float allowedRange;
    //A reference to the enemy.
    public GameObject myEnemy;
    //The speed at which the enemy will follow the player.
    public float enemySpeed;
    // The attack trigger indicates if the the enemy is currently attacking the player.
    public int attackTrigger;
    // The raycast used to check for the player.
    public RaycastHit shot;
    // The amount of hit points that the enemy has.
    public float health;

    public bool isDead;

    public NavMeshAgent agent;

    public BoxCollider hitBox;


	// Update is called once per frame
	void Update () {
		
	}

    void RandomNavmeshLocation(float radius)
    {
        //get a random direction
        Vector3 randomDirection = (Random.insideUnitSphere * radius) + transform.position;

        //used to check for a hit
        NavMeshHit hit;
        //if we have a 
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            //set the destination for the navMeshAgent
            agent.SetDestination(hit.position);
        }
    }

    public void TakeDamage(int amount)
    {
        // If the enemy is dead...
        if (isDead)
            // ... no need to take damage so exit the function.
            return;

        

        // Reduce the current health by the amount of damage sustained.
        health -= amount;

        // If the current health is less than or equal to zero...
        if (health <= 0)
        {
            // ... the enemy is dead.
            Death();
        }
    }

     void Death()
    {
        Destroy(gameObject);
    }
}
