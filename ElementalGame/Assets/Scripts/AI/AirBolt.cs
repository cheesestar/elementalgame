﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class AirBolt : MonoBehaviour {
    
    public int myDamage;
    private float currentTime;
    private float maxTime = 3;
    // Use this for initialization
    void Start()
    {
        myDamage = 1;
    }

    private void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= maxTime)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("hit");
            other.GetComponent<HealthSystem>().TakeDamage(myDamage);
            if (other.GetComponent<HealthSystem>().currentHitpoints == 0)
            {
                other.GetComponent<HealthSystem>().dieByEnemy = true;
            }
            Destroy(this.gameObject);
        }
    }  

}