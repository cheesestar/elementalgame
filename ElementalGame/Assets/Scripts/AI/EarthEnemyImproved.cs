﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// Author: Jemma Thompson
public class EarthEnemyImproved : MonoBehaviour {
    Transform thePlayer;
    HealthSystem playerHealth;
    int myMaxHitpoints;
    public int myCurrentHitpoints;
    bool playerInRange;
    NavMeshAgent nav;
    public float timeBetweenAttacks = 1f;
    public int attackDamage = 1;
    Animator myAC;
    float timer;
    bool imDead;
    public GameObject essenceDrop;
    public Renderer myRenderer;

    void Awake()
    {
        thePlayer = GameObject.FindGameObjectWithTag("Player").transform;
        playerHealth = thePlayer.GetComponent<HealthSystem>();
        myRenderer = GetComponentInChildren<Renderer>();
        myMaxHitpoints = 2;
        myCurrentHitpoints = myMaxHitpoints;
        nav = GetComponent<NavMeshAgent>();
        myAC = GetComponent<Animator>();
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= timeBetweenAttacks && playerInRange && myCurrentHitpoints > 0)
        {
            myAC.Play("Attack");

        }
        if (myCurrentHitpoints > 0 && playerHealth.currentHitpoints > 0)
        {
            nav.SetDestination(thePlayer.position);
        }
        else
        {
            //nav.enabled = false;
        }

    }

    public void TakeDamage(int damage)
    {
        if (imDead)
            return;
        myCurrentHitpoints -= damage;
        StartCoroutine("FlashRed");
        if (myCurrentHitpoints <= 0)
        {
            Die();
        }
    }

    public IEnumerator FlashRed()
    {
        for (int i = 0; i < 4; i++)
        {
            myRenderer.material.color = Color.red;
            yield return new WaitForSeconds(0.1f);
            myRenderer.material.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerInRange = true;

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerInRange = false;
        }
    }

    void Attack()
    {
        timer = 0f;
        if (playerHealth.currentHitpoints > 0 && playerInRange == true)
        {

            thePlayer.GetComponent<HealthSystem>().TakeDamage(attackDamage);
            if (thePlayer.GetComponent<HealthSystem>().currentHitpoints == 0)
            {
                thePlayer.GetComponent<HealthSystem>().dieByEnemy = true;
            }
        }
    }

    void Die()
    {
        imDead = true;
        gameObject.GetComponent<CapsuleCollider>().isTrigger = true;
        myAC.Play("Die");
    }
    public void DestroySelf()
    {
        DropItems();
        Destroy(this.gameObject);
    }

    public void DropItems()
    {
        Instantiate(essenceDrop, new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y+10, this.gameObject.transform.position.z), essenceDrop.transform.rotation);
        Instantiate(essenceDrop, new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y+10, this.gameObject.transform.position.z+30), essenceDrop.transform.rotation);
        Instantiate(essenceDrop, new Vector3(this.gameObject.transform.position.x+25, this.gameObject.transform.position.y+10, this.gameObject.transform.position.z+15), essenceDrop.transform.rotation);
    }
}

