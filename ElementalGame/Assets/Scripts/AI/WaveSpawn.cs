﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Written by Brodie Pirovich

public class WaveSpawn : MonoBehaviour {

    public enum spawnState { Spawning, Waiting, Counting, Nothing}

    [System.Serializable]
    public class Wave
    {
        public string name;
        public GameObject enemy;
        public int count;
        public float rate;
    }

    public Wave[] waves;
    public Transform[] spawnPoints;

    private int nextWave;
    public float timeBetweenWaves = 5;
    private float waveCountDown;
    private float searchCountDown = 1;
    public GameObject reward;

    private spawnState state = spawnState.Nothing;


    void Start()
    {
        reward.gameObject.SetActive(false);
        waveCountDown = timeBetweenWaves;
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points placed");
        }
    }

    void Update()
    {
        if(state == spawnState.Waiting)
        {
            if (!enemyIsAlive())
            {

            }
            else
            {
                return;
            }
        }

        if (waveCountDown <= 0)
        {
            if (state != spawnState.Spawning)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            waveCountDown -= Time.deltaTime;
        }
    }

    void WaveComplete()
    {
        reward.gameObject.SetActive(true);
    }

    bool enemyIsAlive()
    {
        searchCountDown -= Time.deltaTime;
        if (searchCountDown <= 0)
        {
            searchCountDown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }

        return true;
    }

    IEnumerator SpawnWave(Wave _wave)
    {
        state = spawnState.Spawning;
        for (int i = 0; i < _wave.count; i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1F / _wave.rate);
        }


        state = spawnState.Waiting;

        yield break;
    }

    void SpawnEnemy(GameObject _enemy)
    {
        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(_enemy, _sp.position, _sp.rotation);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            state = spawnState.Counting;
            Debug.Log("Collision");
        }
    }
}
