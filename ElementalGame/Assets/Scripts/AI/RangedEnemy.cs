﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Written by Brodie Pirovich

public class RangedEnemy : EnemyClass
{
    [SerializeField]
    private float projectileSpeed;
    [SerializeField]
    private GameObject projectilePrefab;
    [SerializeField]
    private GameObject tempProjectile;
    [SerializeField]
    private float nextFire = 0f;
    [SerializeField]
    private float fireRate = 1f;

    private void Start()
    {
        health = 50f;
        allowedRange = 1000;
        thePlayer = GameObject.FindGameObjectWithTag("Player").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(thePlayer.transform);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out shot))
        {
            targetDistance = shot.distance;
            if (targetDistance < allowedRange && Time.time >= nextFire)
            {
                enemySpeed = 550000f;
                if (attackTrigger == 0)
                {
                    //myEnemy.GetComponent<Animation>().Play("Walking");
                    //transform.position = Vector3.MoveTowards(transform.position, thePlayer.transform.position, enemySpeed);
                    //agent.SetDestination(Vector3.MoveTowards(transform.position, thePlayer.transform.position, enemySpeed));

                    if (targetDistance <= 600)
                    {
                        attackTrigger = 1;
                    }
                }
            }
            else
            {
                enemySpeed = 0;
                //myEnemy.GetComponent<Animation>().Play("Idle");
            }

        }

        if (attackTrigger == 1 && Time.time >= nextFire)
        {
            nextFire = Time.time + 1f / fireRate;
            Shoot();
            //myEnemy.GetComponent<Animation>().Play("Attacking");
            attackTrigger = 0;
        }
    }

    private void Shoot()
    {
        tempProjectile = Instantiate(projectilePrefab, transform.position, transform.rotation);
        var rb = tempProjectile.GetComponent<Rigidbody>();
        rb.AddForce(tempProjectile.transform.forward * projectileSpeed);
        Destroy(tempProjectile, 3);
    }

}
