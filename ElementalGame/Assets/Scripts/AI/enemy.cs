﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Written by Brodie Pirovich

public class enemy : EnemyClass
{
    [SerializeField]
    private float nextAttack = 0f;
    [SerializeField]
    private float attackRate = 0.5f;

    private void Start()
    {
        playerHealth = thePlayer.GetComponent<HealthSystem>();
        health = 100;

        allowedRange = 300f;
        thePlayer = GameObject.FindGameObjectWithTag("Player").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(Vector3.MoveTowards(transform.position, thePlayer.transform.position, enemySpeed));

        transform.LookAt(thePlayer.transform);
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out shot))
        {
            targetDistance = shot.distance;
            if (targetDistance < allowedRange)
            {
                enemySpeed = 1500000f;
                if (attackTrigger == 0)
                {
                    //myEnemy.GetComponent<Animation>().Play("Walking");
                    //transform.position = Vector3.MoveTowards(transform.position, thePlayer.transform.position, enemySpeed);
                    agent.SetDestination(Vector3.MoveTowards(transform.position, thePlayer.transform.position, enemySpeed));

                    if (targetDistance <= 2)
                    {
                        attackTrigger = 1;
                    }
                }
            }
            else
            {
                enemySpeed = 0;
                //myEnemy.GetComponent<Animation>().Play("Idle");
            }

        }

        if (attackTrigger == 1 && Time.time >= nextAttack)
        {
            nextAttack = Time.time + 1f / attackRate;
            Attack();
            attackTrigger = 0;
        }
    }

    void Attack()
    {
        //myEnemy.GetComponent<Animation>().Play("Attacking");
        Debug.Log("Attack");
        playerHealth.currentHitpoints--;
    }
}


