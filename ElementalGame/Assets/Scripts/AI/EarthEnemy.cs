﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EarthEnemy : MonoBehaviour {

    public float lookRadius = 150000f;
    public Transform target;
    NavMeshAgent agent;
    public HealthSystem playerHealth;
    public int attackTrigger;


    // Use this for initialization
    void Start () {
        agent = GetComponent<NavMeshAgent>();
        playerHealth = target.GetComponent<HealthSystem>();
    }

    // Update is called once per frame
    void Update () {
        float distance = Vector3.Distance(target.position, transform.position);
        //myEnemy.GetComponent<Animation>().Play("Idle");

        if (distance <= lookRadius)
        {
            attackTrigger = 1;
            agent.SetDestination(target.position);

            if(distance >= agent.stoppingDistance)
            {
                Attack();
            }
        }

        if(attackTrigger == 1)
        {
            //myEnemy.GetComponent<Animation>().Play("Walking");
        }
    }

    void Attack()
    {
        Debug.Log("Attack");
        //GetComponent<Animation>().Play("Attacking");
        playerHealth.currentHitpoints--;
    }

    void FaceTarget ()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5000f);

    }

    void OnDrawGizmoSelected ()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}
