﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public Image[] hitPointImages;
    public Image[] earthPowerImages;
    public Image[] airPowerImages;
    public GameObject thePlayer;
    public Text playerLives;
    public Text essenceDrops;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        playerLives.text = thePlayer.GetComponent<HealthSystem>().livesCount.ToString();
        essenceDrops.text = GetComponent<InventoryScript>().coins.ToString();
        switch (thePlayer.GetComponent<HealthSystem>().currentHitpoints)
        {
            case 3:
                hitPointImages[0].enabled = true;
                hitPointImages[1].enabled = true;
                hitPointImages[2].enabled = true;
                break;
            case 2:
                hitPointImages[0].enabled = true;
                hitPointImages[1].enabled = true;
                hitPointImages[2].enabled = false;
                break;
            case 1:
                hitPointImages[0].enabled = true;
                hitPointImages[1].enabled = false;
                hitPointImages[2].enabled = false;
                break;
            case 0:
                hitPointImages[0].enabled = false;
                hitPointImages[1].enabled = false;
                hitPointImages[2].enabled = false;
                break;

        }

        switch (thePlayer.GetComponent<ElementalPowers>().currentEarthAmount)
        {
            case 3:
                earthPowerImages[0].enabled = true;
                earthPowerImages[1].enabled = true;
                earthPowerImages[2].enabled = true;
                break;
            case 2:
                earthPowerImages[0].enabled = true;
                earthPowerImages[1].enabled = true;
                earthPowerImages[2].enabled = false;
                break;
            case 1:
                earthPowerImages[0].enabled = true;
                earthPowerImages[1].enabled = false;
                earthPowerImages[2].enabled = false;
                break;
            case 0:
                earthPowerImages[0].enabled = false;
                earthPowerImages[1].enabled = false;
                earthPowerImages[2].enabled = false;
                break;

        }
        
        switch (thePlayer.GetComponent<ElementalPowers>().currentAirAmount)
        {
            case 3:
                airPowerImages[0].enabled = true;
                airPowerImages[1].enabled = true;
                airPowerImages[2].enabled = true;
                break;
            case 2:
                airPowerImages[0].enabled = true;
                airPowerImages[1].enabled = true;
                airPowerImages[2].enabled = false;
                break;
            case 1:
                airPowerImages[0].enabled = true;
                airPowerImages[1].enabled = false;
                airPowerImages[2].enabled = false;
                break;
            case 0:
                airPowerImages[0].enabled = false;
                airPowerImages[1].enabled = false;
                airPowerImages[2].enabled = false;
                break;

        }        

    }
}
