﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma
public class RangedSpell : MonoBehaviour
{
    public int myDamage;
    private float currentTime;
    private float maxTime = 3;
    // Use this for initialization
    void Start()
    {
        myDamage = 1;
    }

    private void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime >= maxTime)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("hit");
            other.gameObject.GetComponent<EarthEnemyImproved>().TakeDamage(myDamage);
            Destroy(this.gameObject);

            //add nockback?
        }
        else if (other.gameObject.tag == "RangeEnemy")
        {
            Debug.Log("hit");
            other.gameObject.GetComponent<AirEnemy>().TakeDamage(myDamage);
            Destroy(this.gameObject);

            //add nockback?
        }
    }

}

