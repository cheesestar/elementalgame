﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
// Written by Brodie Pirovich

public class InventoryScript : MonoBehaviour
{

    public float coins;
    public float gems;
    public float windKey;
    public float earthKey;
    public float level;
    public Scene currentScene;
    public string levelName;
    public HealthSystem health;

    // Use this for initialization
    void Start()
    {
        currentScene = SceneManager.GetSceneByName("Scene Name");
        health = GameObject.Find("ameria").GetComponent<HealthSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentScene == SceneManager.GetSceneByName("EarthLevelGreyBox"))
        {
            level = 1;
        }

        if (currentScene == SceneManager.GetSceneByName("MainMenu"))
        {
            level = 0;
        }

        if (coins >= 50)
        {
            coins = 0;
            health.livesCount++;
        }

    }

    public void Save()
    {
        SaveScript.Save(this);
        SaveScript.SaveLevel(health);
        SaveScript.SaveCheckpoint(health);
    }

    public void Load()
    {
        float[] loadedStats = SaveScript.Load();

        coins = loadedStats[0];
        gems = loadedStats[1];
        windKey = loadedStats[2];
        earthKey = loadedStats[3];
        level = loadedStats[4];

        int[] loadedLevelStats = SaveScript.LoadLevel();

        health.currentHitpoints = loadedLevelStats[0];
        health.currentHitpoints = loadedLevelStats[1];

        Transform[] loadedCheckpointStats = SaveScript.LoadCheckpoint();

        health.myCheckpoint = loadedCheckpointStats[0];
    }
}
