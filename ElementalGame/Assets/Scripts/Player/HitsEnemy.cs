﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class HitsEnemy : MonoBehaviour
{
    public int myDamage;
    public MeshCollider myCollider;
    // Use this for initialization
    void Start()
    {
        myDamage = 1;
        myCollider = GetComponent<MeshCollider>();
        myCollider.enabled = false;
    }
  
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Debug.Log("hit");
            other.gameObject.GetComponent<EarthEnemyImproved>().TakeDamage(myDamage);
            

            //add nockback?
        }
        if (other.gameObject.tag == "RangeEnemy")
        {
            Debug.Log("hit");
            other.gameObject.GetComponent<AirEnemy>().TakeDamage(myDamage);

            //add nockback?
        }
    }

}
