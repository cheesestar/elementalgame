﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class HealthSystem : MonoBehaviour
{
    public Animator playerAnimation;
    [SerializeField]
    public int currentHitpoints;
    private int maxHitpoints = 3;
    [SerializeField]
    public int livesCount;
    public bool dieByEnemy = false;
    public bool dieByFalling;
    public Transform mySpawnpoint;
    public Transform myCheckpoint;
    public Renderer myRenderer;
    public PlayerSFXController playerSounds;
    public int essenceDrop;
    // Use this for initialization
    
    void Start()
    {
        playerAnimation = GetComponent<Animator>();
        myRenderer = GetComponentInChildren<Renderer>();
        playerSounds = GetComponent<PlayerSFXController>();
        currentHitpoints = maxHitpoints;
        livesCount = 3;
    }

    // Update is called once per frame
    void Update()
    {       
        switch (currentHitpoints)
        {
            case 3:                
                break;
            case 2:

                break;
            case 1:

                break;
            case 0:               
                if (dieByEnemy)
                {
                    playerAnimation.Play("Death (Enemy)");
                }
                else
                {
                    Die();
                    //playerAnimation.Play("Death (Falling)");
                }
                break;

        }

    }
    public void TakeDamage(int damage)
    {
        currentHitpoints -= damage;
        StartCoroutine("FlashRed");
        if (dieByEnemy || dieByFalling)
        {
            return;
        }
        if (currentHitpoints <= 0)
        {
            if (dieByEnemy == true)
            {
                playerAnimation.Play("Death (Enemy)");
            }
            else
            {
                //playerAnimation.Play("Death (Falling)");
                Die();
            }
            
        }
    }
    public void Die()
    {
        livesCount--;       
        if (livesCount < 0)
        {
            //player gets a game over screen that makes the player choose to reload a save, go back to menu or quit
        }
        else
        {
            
            currentHitpoints = maxHitpoints;
            transform.position = mySpawnpoint.position;
            playerAnimation.Play("Movement");
            dieByEnemy = false;
            dieByFalling = false;
            //player gets put back to a spawn point
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Spawnpoint")
        {
            mySpawnpoint = other.gameObject.transform;
        }
        if (other.gameObject.tag == "Checkpoint")
        {
            myCheckpoint = other.gameObject.transform;
        }
        if (other.gameObject.tag == "DeadZone" && myCheckpoint != null)
        {
            Debug.Log("died");
            if (currentHitpoints <= 1)
            {
                dieByFalling = true;
            }
            TakeDamage(1);
            
            
            transform.position = myCheckpoint.position;
        }
        if (other.CompareTag("Essence"))
        {
            playerSounds.GainedItem();
            Destroy(other.gameObject);
            essenceDrop++;
        }
        if (other.CompareTag("Thorns"))
        {
            TakeDamage(1);
        }
        if (other.CompareTag("HealthFruit"))
        {
            if (currentHitpoints < maxHitpoints)
            {
                playerSounds.GainedItem();
                Destroy(other.gameObject);
                currentHitpoints++;
            }
           
        }
    }
     public IEnumerator FlashRed()
    {
        for (int i = 0; i < 4; i++)
        {
            myRenderer.material.color = Color.red;
            yield return new WaitForSeconds(0.1f);
            myRenderer.material.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }      
    }
}
