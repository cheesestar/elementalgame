﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class PlayerController : MonoBehaviour
{
    // created a state machine with glide, movment etc
    public enum States { Moving, Gliding, Dieing, Combat, Airborn, Paused }
    public States myState;
    public float onSurfaceSphereSize;
    [SerializeField]
    private float playerSpeed;
    private Vector3 playerMotion;
    public CharacterController myController;
    public Animator playerAnimation;
    public float jumpHeight;
    public float myGravity;
    public Vector3 myVelocity;
    public GameObject playerSpine;
    [SerializeField]
    private float currentRotation;
    [SerializeField]
    private float rotationSpeed;
    private Quaternion lastRot;
    [SerializeField]
    private float lastRotationY;
    private float surfaceRange = 0.9f;
    [SerializeField]
    private bool onSurface = false;
    public bool isGliding = false;
    public CombatSystem combatSystem;
    public bool isJumping;
    public GameObject cloudObject;
    public bool isPaused;
    private PlayerSFXController playerSFXs;
    private HealthSystem playerHealth;
    public bool cameraBehind;
    // Use this for initialization
    void Start()
    {
        myState = States.Moving;
        combatSystem = gameObject.GetComponent<CombatSystem>();
        playerSFXs = gameObject.GetComponent<PlayerSFXController>();
        playerHealth = gameObject.GetComponent<HealthSystem>();
        cloudObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {

        //RaycastHit hit = new RaycastHit();
        if (/*Physics.SphereCast(transform.position + Vector3.up, onSurfaceSphereSize, Vector3.down, out hit, surfaceRange)*/ myController.isGrounded) // when the player detects that it's on a platform or ground sets grounded to true.
        {
            onSurface = true;

            myState = States.Moving;
            Debug.Log("grounded");
            if (playerHealth.currentHitpoints == 0)
            {
                myState = States.Dieing;
            }
        }
        else
        {
            onSurface = false;
            //myState = States.Airborn;           
        }

        if (Input.GetButton("Pause"))
        {
            isPaused = true;
        }
        if (isPaused == true)
        {
            //myState = States.Paused;
        }

        switch (myState)
        {
            case States.Moving:
                Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
                playerMotion = Quaternion.AngleAxis(Camera.main.transform.eulerAngles.y, Vector3.up) * input * playerSpeed;
                combatSystem.enabled = true;
                if (onSurface && !isJumping)
                {

                    myVelocity.y = -myGravity * Time.deltaTime;
                    //myVelocity.y = 0;
                    if (Input.GetButtonDown("Jump"))
                    {
                        //playerAnimation.Play("Jump");
                        playerAnimation.SetBool("jumping", true);
                        isJumping = true;
                    }
                    else
                    {
                        playerAnimation.SetBool("jumping", false);
                        isJumping = false;
                        myVelocity.y -= (myGravity * Time.deltaTime);                       
                    }
                    if (Input.GetButton("Sprint"))
                    {

                        playerSpeed = 240;

                    }
                    else
                    {
                        playerSpeed = 120;
                    }
                }
                else if (myVelocity.y >= jumpHeight)
                {
                    isJumping = false;
                    //myState = States.Airborn;
                }
                cloudObject.SetActive(false);
                playerAnimation.SetBool("gliding", false);
                playerAnimation.SetFloat("movespeed", playerMotion.magnitude / playerSpeed);
                //combatSystem.myWeapon.SetActive(false);
                break;
            case States.Gliding:
                isJumping = false;
                playerAnimation.SetBool("gliding", true);
                playerSFXs.Glide();
                input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
                playerMotion = Quaternion.AngleAxis(Camera.main.transform.eulerAngles.y, Vector3.up) * input * playerSpeed;
                cloudObject.SetActive(true);
                break;                            
            case States.Dieing:
                input = new Vector3(0, 0f, 0);
                playerMotion = new Vector3(0, 0, 0);
                playerAnimation.SetFloat("movespeed", 0f);
                combatSystem.enabled = false;
                cloudObject.SetActive(false);
                break;
        }

        if (playerMotion != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(playerMotion), 0.10f);
        }
        myController.Move(playerMotion * Time.deltaTime);
        myVelocity.y -= (myGravity * Time.deltaTime);
        myController.Move(myVelocity * Time.deltaTime);
    }

    public void Jump()
    {
        if (isJumping == true)
        {
            myVelocity.y = jumpHeight;
        }
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position + Vector3.up, onSurfaceSphereSize);
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("SnapCamera"))
        {
            cameraBehind = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("SnapCamera"))
        {
            cameraBehind = false;
        }
    }

}
