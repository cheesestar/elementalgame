﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author Jemma Thompson
public class PlayerSFXController : MonoBehaviour {
   public AudioClip[] playerSFXs;
   private AudioSource playerSource;
    // Use this for initialization
    void Start() {
        playerSource = GetComponent<AudioSource>();
    }
    public void SpellCast()
    {
        playerSource.time = 2f;
        playerSource.PlayOneShot(playerSFXs[0], 1f);       
    }
    public void GainedItem()
    {
        playerSource.PlayOneShot(playerSFXs[1], 1f);
    }
    public void MeleeAttack()
    {
        playerSource.PlayOneShot(playerSFXs[2], 1f);
    }
    public void FootStep()
    {
        playerSource.PlayOneShot(playerSFXs[3], 1f);
    }
    public void Glide()
    {
        if (!playerSource.isPlaying)
        {
            playerSource.PlayOneShot(playerSFXs[4], 0.2f);
        }
        
    }
    public void StopSound()
    {
        if (playerSource.isPlaying)
        {
            playerSource.Stop();
        }
        
    }
}
