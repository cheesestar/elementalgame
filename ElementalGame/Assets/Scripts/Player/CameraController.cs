﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class CameraController : MonoBehaviour {
    public GameObject pivotOb;
    private Vector3 offSet;
    [SerializeField]
    private float orbitDampen = 10f;
    [SerializeField]
    private float cameraSpeed = 5f;
    public GameObject myPlayer;
    private void Start()
    {
        myPlayer = GameObject.Find("ameria");
    }
    private void Update()
    {
        if (myPlayer.GetComponent<PlayerController>().cameraBehind == true)
        {

        }
        else
        {

        }
    }
    // Update is called once per frame
    void LateUpdate () {
        if (myPlayer.GetComponent<PlayerController>().cameraBehind == true)
        {
            offSet.y = 40f;
            offSet.x = 0f;
        }
        else
        {
            offSet.y += Input.GetAxis("Right Y") * cameraSpeed;
            offSet.x += Input.GetAxis("Right X") * cameraSpeed;
            if (offSet.y < -50f)
            {
                offSet.y = -40f;
            }
            else if (offSet.y > 89f)
            {
                offSet.y = 89f;
            }
            Quaternion qT = Quaternion.Euler(offSet.y, offSet.x, 0);
            pivotOb.transform.rotation = Quaternion.Lerp(pivotOb.transform.rotation, qT, Time.deltaTime * orbitDampen);
        }
        
    }
}
