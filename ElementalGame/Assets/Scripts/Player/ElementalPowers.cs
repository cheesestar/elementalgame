﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Author: Jemma Thompson
public class ElementalPowers : MonoBehaviour
{

    public GameObject[] leafPlant;
    private PlayerSFXController playerSounds;
    private int earthAmount = 0;
    public int currentEarthAmount;
    public int airAmount = 0;
    public int currentAirAmount;
    public int leafInt;
    public Material leafWilt;
    public Color leafColour;
    // refernces to leaf platforms
    public GameObject leafReference;
    [SerializeField]
    private GameObject hitObject;
    RaycastHit objectHit;
    public GameObject selectedObject;
    public LayerMask layerMask;
    public float rayDistance;
    public float glideTimer;
    public float glideMaxtime;
    public float secondAirTime = 3.1f;
    public float thirdAirTime = 5.1f;
    public bool secondAirUsed;
    public bool thirdAirUsed;
    public bool airPressed;
    public PlayerController playerController;
    public DestroyPlatform destroyPlatform;
    private Animator playerAnimations;
    // Use this for initialization
    void Start()
    {
        playerController = gameObject.GetComponent<PlayerController>();
        playerSounds = gameObject.GetComponent<PlayerSFXController>();
        playerAnimations = gameObject.GetComponent<Animator>();
        leafPlant = new GameObject[3];
    }

    // Update is called once per frame
    void Update()
    {
        if (currentEarthAmount >= 1)
        {
            //change to sphere cast to allow easier less percise aiming
            Ray myRay = new Ray(this.gameObject.transform.position + new Vector3(0, 65f, 0), Camera.main.transform.forward * rayDistance);
            if (Physics.SphereCast(myRay, 30f, out objectHit, rayDistance, layerMask.value))
            {
                if (objectHit.collider != null)
                {
                    hitObject = objectHit.collider.gameObject;
                    SelectObject(hitObject);
                    //hitObject.GetComponent<Renderer>().material.color = Color.yellow;
                }
            }
            else
            {
                ClearSelection();
            }
            //Physics.Raycast(myRay, out objectHit, rayDistance, layerMask.value);

            //hitObject.GetComponent<Renderer>().material.color = Color.white;
            Debug.DrawRay(/*Camera.main.transform.position*/this.gameObject.transform.position + new Vector3(0, 0f, 0), Camera.main.transform.forward * rayDistance, Color.red);
            Debug.DrawRay(/*Camera.main.transform.position*/this.gameObject.transform.position + new Vector3(0, 65f, 0), Camera.main.transform.forward * rayDistance, Color.yellow);
            if (Input.GetButtonDown("Fire3"))
            {
                playerAnimations.Play("Leaf Ability");
                //EarthPower();
            }
        }
        if (currentEarthAmount == 0)
        {
            ClearSelection();
        }
        if (currentEarthAmount >= earthAmount)
        {
            currentEarthAmount = earthAmount;
        }
        if (airAmount >= 1)
        {
            if (Input.GetButtonDown("Fire3") && playerController.myVelocity.y <= 150f)
            {
                glideTimer = 0;
                airPressed = true;
                currentAirAmount--;
                //playerSounds.Glide();
                AirPower();
            }
            else
            {
                playerController.myGravity = 1000f;
            }
            if (airPressed == true)
            {
                glideTimer += Time.deltaTime;

                
                if (glideTimer < glideMaxtime)
                {
                    playerController.isGliding = true;
                    playerController.myGravity = -1f;
                    playerController.myState = PlayerController.States.Gliding;
                    if ((glideTimer > secondAirTime) && secondAirUsed == false)
                    {
                        currentAirAmount--;
                        secondAirUsed = true;

                    }
                    if ((glideTimer > thirdAirTime) && thirdAirUsed == false)
                    {
                        currentAirAmount--;
                        thirdAirUsed = true;

                    }

                }
                else
                {
                    currentAirAmount = airAmount;
                    playerController.myGravity = 1000f;
                    playerController.isGliding = false;
                    playerSounds.StopSound();
                    airPressed = false;
                    secondAirUsed = false;
                    thirdAirUsed = false;
                    playerController.myState = PlayerController.States.Moving;
                }               
            }
        }
    }

    void SelectObject(GameObject obj)
    {
        if (selectedObject != null)
        {
            if (obj == selectedObject)
                return;
            ClearSelection();
        }
        selectedObject = obj;
        Renderer[] rs = selectedObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer r in rs)
        {
            Material m = r.material;
            m.color = Color.magenta;
            r.material = m;
        }
    }

    void ClearSelection()
    {
        if (selectedObject == null)
            return;
        Renderer[] rs = selectedObject.GetComponentsInChildren<Renderer>();
        if (selectedObject.layer == 9)
        {
            foreach (Renderer r in rs)
            {
                Material m = r.material;
                m.color = leafColour;
                r.material = m;
            }
        }
        else
        {
            foreach (Renderer r in rs)
            {
                Material m = r.material;
                m.color = Color.white;
                r.material = m;
            }
        }
        
        selectedObject = null;
    }
    // change to collecting script
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EarthPower"))
        {
            playerSounds.GainedItem();
            Destroy(other.gameObject);
            earthAmount++;
            currentEarthAmount++;
        }
        if (other.CompareTag("AirPower"))
        {
            playerSounds.GainedItem();
            Destroy(other.gameObject);
            airAmount++;
            currentAirAmount++;
        }
    }

    public void EarthPower()
    {
        if (hitObject != null)
        {
            if (currentEarthAmount >= 1 && hitObject.tag == "LeafBulb")
            {
                LeafSpawn();
            }
            else if (currentEarthAmount >= 1 && (hitObject.tag == "Leaf" || hitObject.tag == "Leaf1"))
            {
                LeafGrow();
            }
            else if (currentEarthAmount == 1 && hitObject.tag == "Leaf2")
            {
                LeafGrowAgain();
            }
        }
    }

    public void AirPower()
    {        
        switch (airAmount)
        {
            case 0:
                break;
            case 1:
                glideMaxtime = 3f;
                break;
            case 2:
                glideMaxtime = 5f;
                break;
            case 3:
                glideMaxtime = 7f;
                break;
        }
    }

    public void LeafSpawn()
    {
        currentEarthAmount--;
        if (leafPlant[0] == null)
        {
            leafPlant[0] = Instantiate(leafReference, new Vector3(hitObject.transform.position.x, hitObject.transform.position.y + 21f, hitObject.transform.position.z), Quaternion.Euler(-0, 90, 0));
            hitObject = null;
            leafPlant[0].AddComponent<DestroyPlatform>();            
            foreach (Transform child in leafPlant[0].transform.GetComponentsInChildren<Transform>())
            {
                child.tag = "Leaf";
            }
        }
        else if (leafPlant[1] == null)
        {
            leafPlant[1] = Instantiate(leafReference, new Vector3(hitObject.transform.position.x, hitObject.transform.position.y + 21f, hitObject.transform.position.z), Quaternion.Euler(-0, 90, 0));
            hitObject = null;
            leafPlant[1].AddComponent<DestroyPlatform>();
            foreach (Transform child in leafPlant[1].transform.GetComponentsInChildren<Transform>())
            {
                child.tag = "Leaf1";
            }
        }
        else if (leafPlant[2] == null)
        {
            leafPlant[2] = Instantiate(leafReference, new Vector3(hitObject.transform.position.x, hitObject.transform.position.y + 21f, hitObject.transform.position.z), Quaternion.Euler(-0, 90, 0));
            hitObject = null;
            leafPlant[2].AddComponent<DestroyPlatform>();
        }
    }
    public void LeafGrow()
    {
        Debug.Log("grow leaf 1");

        if (hitObject.tag == "Leaf")
        {
            Debug.Log("grow leaf");
            destroyPlatform = leafPlant[0].GetComponent<DestroyPlatform>();
            destroyPlatform.currentTime = 0;
            leafPlant[0].transform.GetChild(1).gameObject.SetActive(true);
            //leafPlant[0].transform.GetChild(4).gameObject.SetActive(true);
            leafPlant[0].tag = "Leaf2";
            foreach (Transform child in leafPlant[0].transform.GetComponentsInChildren<Transform>())
            {
                child.tag = "Leaf2";
            }
        }
        else if (hitObject.tag == "Leaf1")
        {
            Debug.Log("grow leaf 2");
            destroyPlatform = leafPlant[1].GetComponent<DestroyPlatform>();
            destroyPlatform.currentTime = 0;
            leafPlant[1].transform.GetChild(1).gameObject.SetActive(true);
            //leafPlant[1].transform.GetChild(4).gameObject.SetActive(true);
            leafPlant[1].tag = "Leaf2";
            foreach (Transform child in leafPlant[1].transform.GetComponentsInChildren<Transform>())
            {
                child.tag = "Leaf2";
            }
        }
        hitObject = null;
        currentEarthAmount--;
    }

    public void LeafGrowAgain()
    {
        Debug.Log("grow leaf again");
        hitObject = null;
        if (leafPlant[0] != null)
        {
            destroyPlatform = leafPlant[0].GetComponent<DestroyPlatform>();
            destroyPlatform.currentTime = 0;
            leafPlant[0].tag = "Leaf3";
            leafPlant[0].transform.GetChild(2).gameObject.SetActive(true);
            //leafPlant[0].transform.GetChild(5).gameObject.SetActive(true);
        }
        currentEarthAmount--;
    }

}
