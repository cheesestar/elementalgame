﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Author: Jemma Thompson
public class CombatSystem : MonoBehaviour
{
    // private int playerDamage = 5;
    public Animator playerAnimations;
    public Transform playerObject;
    public PlayerController playerController;
    public GameObject myWeapon;
    public GameObject mySpell;
    [SerializeField]
    private GameObject tempSpell;
    public int spellSpeed;
    public Transform mySpawn;
    public Rigidbody myRB;
    private PlayerSFXController playerSounds;
    // Use this for initialization
    void Start()
    {
        playerController = GetComponent<PlayerController>();
        playerAnimations = GetComponent<Animator>();
        playerSounds = GetComponent<PlayerSFXController>();
        //myWeapon.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            //myWeapon.SetActive(true);

            playerAnimations.Play("melee attack");
        }

        //myWeapon.SetActive(false);
        if (Input.GetButtonDown("Fire1"))
        {
            //myWeapon.SetActive(true)
            playerAnimations.Play("SpellCast");
        }
    }

    void RangedAttack()
    {
        tempSpell = Instantiate(mySpell, mySpawn.position, playerObject.rotation);
        playerSounds.SpellCast();
        myRB = tempSpell.GetComponent<Rigidbody>();
        myRB.velocity = transform.forward * 150;
        //
    }
    public void weaponOn()
    {
        myWeapon.GetComponent<MeshCollider>().enabled = true;
    }
    public void weaponOff()
    {
        myWeapon.GetComponent<MeshCollider>().enabled = false;
    }
}

