﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovingPlatform))]
public class MovingPlatformGUI : Editor {

    MovingPlatform movingplatform;

    private void OnEnable()
    {
        movingplatform = (MovingPlatform)target;
    }
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
    }
    private void OnSceneGUI()
    {
        
        movingplatform.startPosition = Handles.PositionHandle(movingplatform.startPosition, Quaternion.identity);
        movingplatform.endPosition = Handles.PositionHandle(movingplatform.endPosition, Quaternion.identity);
        Handles.Label(movingplatform.startPosition, "Start");
        Handles.Label(movingplatform.endPosition, "End");

    }
}
