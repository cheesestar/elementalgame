%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Combat
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: CH_Ameria_GEO
    m_Weight: 0
  - m_Path: CTRL_Ameria
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_L_Foot
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_L_Foot/CTRL_L_Foot003
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_L_Foot/CTRL_L_Foot003/BNE_L_Sole
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_L_Foot/CTRL_L_Foot003/BNE_L_Sole/CTRL_L_Foot002
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_L_Foot/CTRL_L_Foot003/BNE_L_Sole/CTRL_L_Foot002/BNE_L_Foot
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_L_Foot/CTRL_L_Foot003/BNE_L_Sole/CTRL_L_Foot002/BNE_L_Foot/CTRL_L_Foot001
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_L_Foot/CTRL_L_Foot003/BNE_L_Sole/CTRL_L_Foot002/BNE_L_Foot/CTRL_L_Foot001/BNE_L_Foot001
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_L_Foot/CTRL_L_Foot003/BNE_L_Sole/CTRL_L_Foot002/BNE_L_Foot/CTRL_L_Foot001/BNE_L_Foot001/NUB_L_Foot
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_L_Foot/CTRL_L_Foot003/BNE_L_Sole/CTRL_L_Foot002/BNE_L_Foot/CTRL_L_Foot001/BNE_L_Foot001/NUB_L_Foot/IK
      Chain002
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/CTRL_Neck
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/CTRL_Neck/BNE_Neck
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/CTRL_Neck/BNE_Neck/CTRL_Head
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/CTRL_Neck/BNE_Neck/CTRL_Head/BNE_Head
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/CTRL_Neck/BNE_Neck/CTRL_Head/BNE_Head/CTRL_Head001
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/CTRL_Neck/BNE_Neck/CTRL_Head/BNE_Head/CTRL_Head001/BNE_Jaw
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/CTRL_Neck/BNE_Neck/CTRL_Head/BNE_Head/CTRL_Head001/BNE_Jaw/NUB_Jaw
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/CTRL_Neck/BNE_Neck/CTRL_Head/BNE_Head/NUB_Head
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/BNE_L_Clavicle
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/BNE_L_Clavicle/BNE_L_UpperArm
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/BNE_L_Clavicle/BNE_L_UpperArm/BNE_L_LowerArm
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/BNE_L_Clavicle/BNE_L_UpperArm/BNE_L_LowerArm/NUB_L_Arm
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/Point020
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/Point020/CTRL_L_Clavicle
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/Point020/CTRL_L_Clavicle/IK
      Chain010
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/UND_L_Arm00
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/UND_L_Arm00/Point005
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/UND_L_Arm00/UND_L_Arm01
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/UND_L_Arm00/UND_L_Arm01/Point008
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/UND_L_Arm00/UND_L_Arm01/UND_L_Arm02
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point019/UND_L_Arm00/UND_L_Arm01/UND_L_Arm02/UND_L_Arm03
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/BNE_R_Clavicle
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/BNE_R_Clavicle/BNE_R_UpperArm
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/BNE_R_Clavicle/BNE_R_UpperArm/BNE_R_LowerArm
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/BNE_R_Clavicle/BNE_R_UpperArm/BNE_R_LowerArm/BNE_R_Palm
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/Point021
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/Point021/CTRL_R_Clavicle001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/Point021/CTRL_R_Clavicle001/IK
      Chain008
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/UND_R_Arm03
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/UND_R_Arm03/Point023
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/UND_R_Arm03/UND_R_Arm02
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/UND_R_Arm03/UND_R_Arm02/Point022
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/UND_R_Arm03/UND_R_Arm02/UND_R_Arm01
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/BNE_Chest/Point024/UND_R_Arm03/UND_R_Arm02/UND_R_Arm01/UND_R_Arm00
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/BNE_L_Hand(mirrored)
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/BNE_L_Hand(mirrored)/Line001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_IndexF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_IndexF001/BNE_R_IndexF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_IndexF001/BNE_R_IndexF001/CTRL_R_IndexF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_IndexF001/BNE_R_IndexF001/CTRL_R_IndexF002/BNE_R_IndexF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_IndexF001/BNE_R_IndexF001/CTRL_R_IndexF002/BNE_R_IndexF002/CTRL_R_IndexF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_IndexF001/BNE_R_IndexF001/CTRL_R_IndexF002/BNE_R_IndexF002/CTRL_R_IndexF003/BNE_R_IndexF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_IndexF001/BNE_R_IndexF001/CTRL_R_IndexF002/BNE_R_IndexF002/CTRL_R_IndexF003/BNE_R_IndexF003/NUB_R_IndexF
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_MiddleF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_MiddleF001/BNE_R_MiddleF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_MiddleF001/BNE_R_MiddleF001/CTRL_R_MiddleF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_MiddleF001/BNE_R_MiddleF001/CTRL_R_MiddleF002/BNE_R_MiddleF
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_MiddleF001/BNE_R_MiddleF001/CTRL_R_MiddleF002/BNE_R_MiddleF/CTRL_R_MiddleF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_MiddleF001/BNE_R_MiddleF001/CTRL_R_MiddleF002/BNE_R_MiddleF/CTRL_R_MiddleF003/BNE_R_MIddleF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_MiddleF001/BNE_R_MiddleF001/CTRL_R_MiddleF002/BNE_R_MiddleF/CTRL_R_MiddleF003/BNE_R_MIddleF003/NUB_R_MiddleF
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Pinky001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Pinky001/BNE_R_Pinky001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Pinky001/BNE_R_Pinky001/CTRL_R_Pinky002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Pinky001/BNE_R_Pinky001/CTRL_R_Pinky002/BNE_R_Pinky002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Pinky001/BNE_R_Pinky001/CTRL_R_Pinky002/BNE_R_Pinky002/CTRL_R_Pinky003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Pinky001/BNE_R_Pinky001/CTRL_R_Pinky002/BNE_R_Pinky002/CTRL_R_Pinky003/BNE_R_Pinky003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Pinky001/BNE_R_Pinky001/CTRL_R_Pinky002/BNE_R_Pinky002/CTRL_R_Pinky003/BNE_R_Pinky003/NUB_R_Pinky
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_RingF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_RingF001/BNE_R_RingF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_RingF001/BNE_R_RingF001/CTRL_R_RingF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_RingF001/BNE_R_RingF001/CTRL_R_RingF002/BNE_R_RingF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_RingF001/BNE_R_RingF001/CTRL_R_RingF002/BNE_R_RingF002/CTRL_R_RingF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_RingF001/BNE_R_RingF001/CTRL_R_RingF002/BNE_R_RingF002/CTRL_R_RingF003/BNE_R_RingF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_RingF001/BNE_R_RingF001/CTRL_R_RingF002/BNE_R_RingF002/CTRL_R_RingF003/BNE_R_RingF003/NUB_R_RingF
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Thumb001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Thumb001/BNE_R_Thumb001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Thumb001/BNE_R_Thumb001/CTRL_R_Thumb002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Thumb001/BNE_R_Thumb001/CTRL_R_Thumb002/BNE_R_Thumb002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Thumb001/BNE_R_Thumb001/CTRL_R_Thumb002/BNE_R_Thumb002/CTRL_R_Thumb003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Thumb001/BNE_R_Thumb001/CTRL_R_Thumb002/BNE_R_Thumb002/CTRL_R_Thumb003/BNE_R_Thumb003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/CTRL_R_Thumb001/BNE_R_Thumb001/CTRL_R_Thumb002/BNE_R_Thumb002/CTRL_R_Thumb003/BNE_R_Thumb003/NUB_R_Thumb
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/IK
      Chain004
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/IK
      Chain005
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm/NUB_L_Hand(mirrored)
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/BNE_L_Hand
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/BNE_L_Hand/NUB_L_Hand
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_IndexF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_IndexF001/BNE_L_IndexF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_IndexF001/BNE_L_IndexF001/CTRL_L_IndexF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_IndexF001/BNE_L_IndexF001/CTRL_L_IndexF002/BNE_L_IndexF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_IndexF001/BNE_L_IndexF001/CTRL_L_IndexF002/BNE_L_IndexF002/CTRL_L_IndexF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_IndexF001/BNE_L_IndexF001/CTRL_L_IndexF002/BNE_L_IndexF002/CTRL_L_IndexF003/BNE_L_IndexF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_IndexF001/BNE_L_IndexF001/CTRL_L_IndexF002/BNE_L_IndexF002/CTRL_L_IndexF003/BNE_L_IndexF003/NUB_L_IndexF
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_MiddleF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_MiddleF001/BNE_L_MiddleF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_MiddleF001/BNE_L_MiddleF001/CTRL_L_MiddleF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_MiddleF001/BNE_L_MiddleF001/CTRL_L_MiddleF002/BNE_L_MiddleF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_MiddleF001/BNE_L_MiddleF001/CTRL_L_MiddleF002/BNE_L_MiddleF002/CTRL_L_MiddleF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_MiddleF001/BNE_L_MiddleF001/CTRL_L_MiddleF002/BNE_L_MiddleF002/CTRL_L_MiddleF003/BNE_L_MiddleF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_MiddleF001/BNE_L_MiddleF001/CTRL_L_MiddleF002/BNE_L_MiddleF002/CTRL_L_MiddleF003/BNE_L_MiddleF003/NUB_L_MiddleF
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Pinky001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Pinky001/BNE_L_Pinky001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Pinky001/BNE_L_Pinky001/CTRL_L_Pinky002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Pinky001/BNE_L_Pinky001/CTRL_L_Pinky002/BNE_L_Pinky002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Pinky001/BNE_L_Pinky001/CTRL_L_Pinky002/BNE_L_Pinky002/CTRL_L_Pinky003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Pinky001/BNE_L_Pinky001/CTRL_L_Pinky002/BNE_L_Pinky002/CTRL_L_Pinky003/BNE_L_Pinky003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Pinky001/BNE_L_Pinky001/CTRL_L_Pinky002/BNE_L_Pinky002/CTRL_L_Pinky003/BNE_L_Pinky003/NUB_L_Pinky
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_RingF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_RingF001/BNE_L_RingF001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_RingF001/BNE_L_RingF001/CTRL_L_RingF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_RingF001/BNE_L_RingF001/CTRL_L_RingF002/BNE_L_RingF002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_RingF001/BNE_L_RingF001/CTRL_L_RingF002/BNE_L_RingF002/CTRL_L_RingF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_RingF001/BNE_L_RingF001/CTRL_L_RingF002/BNE_L_RingF002/CTRL_L_RingF003/BNE_L_RingF003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_RingF001/BNE_L_RingF001/CTRL_L_RingF002/BNE_L_RingF002/CTRL_L_RingF003/BNE_L_RingF003/NUB_L_RingF
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Thumb001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Thumb001/BNE_L_Thumb001
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Thumb001/BNE_L_Thumb001/CTRL_L_Thumb002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Thumb001/BNE_L_Thumb001/CTRL_L_Thumb002/BNE_L_Thunb002
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Thumb001/BNE_L_Thumb001/CTRL_L_Thumb002/BNE_L_Thunb002/CTRL_L_Thumb003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Thumb001/BNE_L_Thumb001/CTRL_L_Thumb002/BNE_L_Thunb002/CTRL_L_Thumb003/BNE_L_Thumb003
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/CTRL_L_Thumb001/BNE_L_Thumb001/CTRL_L_Thumb002/BNE_L_Thunb002/CTRL_L_Thumb003/BNE_L_Thumb003/NUB_L_Thumb
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/IK Chain006
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Arm
      1/IK Chain009
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_L_Elbow
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/CTRL_Spine001/BNE_Spine001/CTRL_Spine002/BNE_Spine002/CTRL_Chest/CTRL_R_Elbow
    m_Weight: 1
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/NUB_L_Thigh
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/NUB_L_Thigh/BNE_L_Thigh
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/NUB_L_Thigh/BNE_L_Thigh/BNE_L_Calf
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/NUB_L_Thigh/BNE_L_Thigh/BNE_L_Calf/NUB_L_Calf
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/NUB_R_Thigh
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/NUB_R_Thigh/BNE_R_Thigh
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/NUB_R_Thigh/BNE_R_Thigh/BNE_R_Calf
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_Pelvis/BNE_Pelvis/NUB_R_Thigh/BNE_R_Thigh/BNE_R_Calf/NUB_R_Calf
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_R_Foot
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_R_Foot/CTRL_R_Foot003
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_R_Foot/CTRL_R_Foot003/BNE_R_Sole
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_R_Foot/CTRL_R_Foot003/BNE_R_Sole/CTRL_R_Foot002
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_R_Foot/CTRL_R_Foot003/BNE_R_Sole/CTRL_R_Foot002/BNE_R_Foot002
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_R_Foot/CTRL_R_Foot003/BNE_R_Sole/CTRL_R_Foot002/BNE_R_Foot002/CTRL_R_Foot001
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_R_Foot/CTRL_R_Foot003/BNE_R_Sole/CTRL_R_Foot002/BNE_R_Foot002/CTRL_R_Foot001/BNE_R_Foor001
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_R_Foot/CTRL_R_Foot003/BNE_R_Sole/CTRL_R_Foot002/BNE_R_Foot002/CTRL_R_Foot001/BNE_R_Foor001/NUB_R_Foot
    m_Weight: 0
  - m_Path: CTRL_Ameria/CTRL_R_Foot/CTRL_R_Foot003/BNE_R_Sole/CTRL_R_Foot002/BNE_R_Foot002/CTRL_R_Foot001/BNE_R_Foor001/NUB_R_Foot/IK
      Chain001
    m_Weight: 0
