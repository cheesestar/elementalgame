﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// Written by Brodie Pirovich

public class Coin : MonoBehaviour {
    public GameObject gameManager;
    public InventoryScript coinCount;
    public PlayerSFXController playerSounds;

	// Use this for initialization
	void Start () {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        coinCount = gameManager.GetComponent<InventoryScript>();
        playerSounds = GameObject.Find("ameria").GetComponent<PlayerSFXController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "ameria")
        {
            coinCount.coins++;
            playerSounds.GainedItem();
            Debug.Log("Coin Collected");
            Destroy(gameObject);
        }
    }
}
